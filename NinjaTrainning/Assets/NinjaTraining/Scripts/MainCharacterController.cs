﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainCharacterController : MonoBehaviour
{
    private Rigidbody rb;
    private Animator anim;
    private bool canControlled;
    private float inputHorizontal;
    private bool isHoldingDown;
    private int lifeCount;
    private ECharacterStatus currentStatus;
    private bool isInvincibleBuff;

    public float moveSpeed = 50f;
    public float maxSpeed = 200f;

    [Header("InvincibleBuff")]
    public float buffTime = 5.0f;
    public Material invincibleMaterial;

    private bool CanApplyUpdate()
    {
        if(null == rb)
        {
            rb = GetComponent<Rigidbody>();
            return false;
        }
        if(null == anim)
        {
            anim = GetComponent<Animator>();
            return false;
        }

        //canControlled = true;

        //if(Application.platform == RuntimePlatform.Android)
        //{
            //Input.gyro.enabled = true;
        //}
        return true;
    }
	void Start ()
    {
        CanApplyUpdate();
        //canControlled = false;

        //OnBeginGame();
        //NJ_GameController.Current.BeginGame();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (false == CanApplyUpdate() || false == canControlled)
        {
            return;
        }

        if(Input.GetMouseButtonDown(0))
        {
            isHoldingDown = true;
        }
        else if(Input.GetMouseButtonUp(0))
        {
            isHoldingDown = false;
        }

        if(Input.GetKeyUp(KeyCode.F))
        {
            anim.SetTrigger("hurtTrigger");
        }

        inputHorizontal = 0;
        if (true == isHoldingDown)
        {
            //Debug.Log(Screen.width);
            if (Input.mousePosition.x <= Screen.width / 2)
            {
                inputHorizontal = -1;
            }
            else
            {
                inputHorizontal = 1;
            }
            Time.timeScale = 1.0f;
        }
        else
        {
            Time.timeScale = 0.4f;
        }
        //if (Application.platform == RuntimePlatform.Android)
        //{
        //}
        //else
        {
            //inputHorizontal = Input.GetAxis("Horizontal");
        }
        if (0 != inputHorizontal)
        {
            rb.AddForce(Vector3.right * inputHorizontal * moveSpeed);
            anim.SetFloat("speed", inputHorizontal * moveSpeed);
            anim.SetBool("isIdle", false);
        }
        else
        {
            anim.SetFloat("speed", 0);
            anim.SetBool("isIdle", true);
        }
        if(rb.velocity.magnitude > maxSpeed)
        {
            rb.velocity = rb.velocity.normalized * maxSpeed;
        }
	}

    private void OnDeath()
    {
        rb.velocity = Vector3.zero;
        canControlled = false;
        anim.SetTrigger("deathTrigger");
        Time.timeScale = 1.0f;
        NJ_GameController.Current.OnGameOver();
    }

    private IEnumerator UpdateInvincibleBuff()
    {
        SkinnedMeshRenderer skinRenderer = GetComponentInChildren<SkinnedMeshRenderer>();
        Material defaultMaterial = skinRenderer.material;

        isInvincibleBuff = true;
        skinRenderer.material = invincibleMaterial;

        yield return new WaitForSeconds(buffTime);

        isInvincibleBuff = false;
        skinRenderer.material = defaultMaterial;
    }

    public void OnGetHit()
    {
        if(true == isInvincibleBuff)
        {
            return;
        }
        //If player is not normal status then quit function
        if (ECharacterStatus.Normal != currentStatus)
        {
            return;
        }
        // On get hit, player become invincible for a short time
        currentStatus = ECharacterStatus.Invincible;
        // Reduce 1 life point
        lifeCount--;
        NJ_GameController.Current.UpdateLifeUI(lifeCount);
        // If run out of life point, 
        if (lifeCount <= 0)
        {
            // Play dead animation
            OnDeath();
        }
        else
        {
            // Trigger hit animation
            anim.SetTrigger("hurtTrigger");
        }
    }
    
    public void OnBeginGame()
    {
        canControlled = true;
        lifeCount = 3;
        currentStatus = ECharacterStatus.Normal;
        isInvincibleBuff = false;
        
        CanApplyUpdate();
        NJ_GameController.Current.UpdateLifeUI(lifeCount);
        AdManager.Instance.Initialize();
    }

    public void EndHurtAnim()
    {
        currentStatus = ECharacterStatus.Normal;
    }

    public void OnPickUpLife()
    {
        lifeCount = Mathf.Clamp(lifeCount + 1, 0, 5);
        NJ_GameController.Current.UpdateLifeUI(lifeCount);
    }

    public void OnPickUpInvincibleBuff()
    {
        StartCoroutine(UpdateInvincibleBuff());
    }
}
