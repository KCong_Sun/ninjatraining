﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NJ_MainHUD : MonoBehaviour
{
    private bool finishedLoadingScreen;
    public bool FinishedLoadingScreen
    {
        get
        {
            return finishedLoadingScreen;
        }
    }

    public Text txtPlayerName;
    public Text txtScore;
    public GameObject panelGameOver;
    public Text txtGameOverScore;
    public List<GameObject> lifeUI;
    public Image imgLoading;

	// Use this for initialization
	void Start ()
    {
        //NJ_GameController.Current.BeginGame();
        //StartCoroutine(NJ_GameController.Current.LoadGameScene());
        //NJ_GameController.Current.TryAgain();

        panelGameOver.SetActive(false);
    }

    private int CurrentLifeDisplay()
    {
        int returnLifeCount = 0;

        foreach(GameObject currentUI in lifeUI)
        {
            HUD_RotateFlyOut currentHUD = currentUI.GetComponentInChildren<HUD_RotateFlyOut>();
            if(true == currentHUD.IsVisible)
            {
                returnLifeCount++;
            }
        }
        return returnLifeCount;
    }


    public void UpdateLifeUI(int inputLife)
    {
        int currentLife = CurrentLifeDisplay();
        Debug.Log(inputLife + "==" + currentLife);
        if (currentLife == inputLife)
        {
            return;
        }
        if(inputLife > currentLife)
        {
            for(int i = currentLife; i < inputLife; i++)
            {
                HUD_RotateFlyOut currentLifeHUD = lifeUI[i].GetComponentInChildren<HUD_RotateFlyOut>();
                currentLifeHUD.PlayFlyInAnimation();
            }
        }
        else if(inputLife < currentLife)
        {
            for (int i = currentLife-1; i >= inputLife; i--)
            {
                HUD_RotateFlyOut currentLifeHUD = lifeUI[i].GetComponentInChildren<HUD_RotateFlyOut>();
                currentLifeHUD.PlayFlyOutAnimation();
            }
        }
    }
	
    public void UpdateScore()
    {
        txtScore.text = "Score: " + NJ_GameController.Current.PlayerScore;
    }

    public void OnDeath()
    {
        panelGameOver.SetActive(true);
        txtPlayerName.text = "Congratulation " + PlayerPrefs.GetString(NJ_Library.fbUserName);
        txtGameOverScore.text = "You have score " + NJ_GameController.Current.PlayerScore + " points!!!";
        txtScore.gameObject.SetActive(false);
    }

    public void OnBtnTryAgain_Clicked()
    {
        //NJ_GameController.Current.TryAgain();
        AdManager.Instance.ShowAd();
    }
        
    private IEnumerator UpdateLoadingScreen(float beginAlpha, float endAlpha, float initialDelayTime = 0)
    {
        imgLoading.color = new Color(0, 0, 0, beginAlpha);
        yield return new WaitForSeconds(initialDelayTime);
        for(float lerpValue = 0; lerpValue <= 1; lerpValue += 0.15f)
        {
            Debug.Log(lerpValue);
            imgLoading.color = new Color(0, 0, 0, Mathf.Lerp(beginAlpha, endAlpha, lerpValue));
            yield return new WaitForSeconds(0.02f);
        }
        Debug.Log("Finish loading!!!");
        finishedLoadingScreen = true;
    }
    public void ShowLoadingScreen(bool isVisible, bool canLerpAlpha, float initialDelayTime = 0)
    {
        if(true == canLerpAlpha)
        {
            float beginAlpha, endAlpha;
            if (true == isVisible)
            {
                beginAlpha = 0;
                endAlpha = 1;
            }
            else
            {
                beginAlpha = 1;
                endAlpha = 0;
            }
            finishedLoadingScreen = false;
            Debug.Log("Loading alpha " + beginAlpha + "===" + endAlpha);
            StartCoroutine(UpdateLoadingScreen(beginAlpha, endAlpha, initialDelayTime));
        }
        else
        {
            finishedLoadingScreen = true;
            if(true == isVisible)
            {
                imgLoading.color = new Color(0, 0, 0, 1);
            }
            else
            {
                imgLoading.color = new Color(0, 0, 0, 0);
            }
        }

    }
}
