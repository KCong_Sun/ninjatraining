﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoRotate : MonoBehaviour
{
    public float rotateRate = 0.25f;
    public Vector3 rotateAxis = new Vector3(0, 1, 0);

    private void Start()
    {
        
    }
    // Update is called once per frame
    void Update ()
    {
        transform.Rotate(rotateAxis, rotateRate);
	}
}
