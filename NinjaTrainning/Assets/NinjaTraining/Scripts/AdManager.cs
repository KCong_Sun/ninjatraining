﻿using UnityEngine;
using UnityEngine.Monetization;
using System.Collections;
using System.Collections.Generic;

public class AdManager
{
    private string placementID = "rewardedVideo";
    private string gameID = "2989773";
    private bool isTestMode = false;
    private static AdManager instance;
    public static AdManager Instance
    {
        get
        {
            if(null == instance)
            {
                instance = new AdManager();
            }

            return instance;
        }
    }
    private AdManager()
    {}

    private void HandleShowAdResult(ShowResult result)
    {
        if(ShowResult.Finished == result)
        {
            NJ_GameController.Current.TryAgain();
        }
    }
    
    public void Initialize()
    {
        Monetization.Initialize(gameID, isTestMode);
    }

    public void ShowAd()
    {
        ShowAdCallbacks options = new ShowAdCallbacks();
        options.finishCallback = HandleShowAdResult;
        ShowAdPlacementContent ad = (ShowAdPlacementContent)Monetization.GetPlacementContent(placementID);
        ad.Show(options);
    }


}
