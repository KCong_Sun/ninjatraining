﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NJ_GameController: MonoBehaviour
{
    public string _StartUpScene = "StartUpScene";
    public string _GameScene = "GameScene";

    private NJ_MainHUD mainHUD;
    private bool canUpdatePlayer =false;
    private MainCharacterController mainPlayer;

    private static NJ_GameController current;
    public static NJ_GameController Current
    {
        private set { }
        get
        {
            if(null == current)
            {
                //GameObject gameControllerObj = new GameObject("GameControllerObj");
                //current = gameControllerObj.AddComponent<NJ_GameController>();
                current = GameObject.FindObjectOfType<NJ_GameController>();
            }
            return current;
        }
    }

    private int playerScore;
    public int PlayerScore
    {
        private set
        {
            playerScore = value;
        }
        get
        {
            return playerScore;
        }
    }
    
    private void Start()
    {
        mainHUD = GameObject.FindObjectOfType<NJ_MainHUD>();
        mainPlayer = GameObject.FindObjectOfType<MainCharacterController>();
        current = GameObject.FindObjectOfType<NJ_GameController>();
        //StartCoroutine(LoadGameScene());
        StartCoroutine(PostLoadGameScene());
    }

    private IEnumerator UpdateScore()
    {
        while(true == canUpdatePlayer)
        {
            yield return new WaitForSeconds(1);

            playerScore++;
            mainHUD.UpdateScore();
        }
    }

    public void OnGameOver()
    {
        canUpdatePlayer = false;

        /*
        MainCharacterController player = GameObject.FindObjectOfType<MainCharacterController>();
        if(null == player)
        {
            return;
        }
        player.OnGetHit();
        */
        mainHUD.OnDeath();

        ItemGenerator generator = GameObject.FindObjectOfType<ItemGenerator>();
        if(null == generator)
        {
            return;
        }
        generator.SetSpawnSetting(false);

        StopCoroutine(current.UpdateScore());

    }

    public void BeginGame()
    {
        canUpdatePlayer = true;
        StartCoroutine(current.UpdateScore());

        MainCharacterController player = GameObject.FindObjectOfType<MainCharacterController>();
        if (null == player)
        {
            return;
        }
        player.OnBeginGame();

        ItemGenerator generator = GameObject.FindObjectOfType<ItemGenerator>();
        if (null == generator)
        {
            return;
        }
        generator.SetSpawnSetting(true);
        
    }

    public void LoadGameScene()
    {
        SceneManager.LoadScene(_GameScene);
    }

    private IEnumerator PostLoadGameScene()
    {
        mainHUD.ShowLoadingScreen(false, true, 0.3f);
        while (false == mainHUD.FinishedLoadingScreen)
        {
            yield return new WaitForSeconds(0.5f);
        }
        BeginGame();
    }

    public void TryAgain()
    {
        mainHUD.ShowLoadingScreen(true, false);
        LoadGameScene();
    }

    public void UpdateLifeUI(int currentLife)
    {
        mainHUD.UpdateLifeUI(currentLife);
    }
}
