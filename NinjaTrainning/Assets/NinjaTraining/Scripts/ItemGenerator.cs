﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ItemInfomation
{
    public GameObject prefab;
    public float spawnRate;
    public float modifiedSpawnRate;
    public float lastSpawnTime;
    public float fallingSlowness;
    public float modifiedFallingSlowness;
    public float increaseSpawnRate;
    public float lastIncreaseSpawnRate;
    public float playerPosDelta;
}
public class ItemGenerator : MonoBehaviour
{
    private float minX, maxX, y, z;
    private bool canSpawn;

    public ItemInfomation[] itemList;

    public Transform playerChar;

	// Use this for initialization
	void Start ()
    {
        BoxCollider boxCollider = GetComponent<BoxCollider>();
        minX = boxCollider.bounds.min.x;
        maxX = boxCollider.bounds.max.x;
        y = boxCollider.bounds.center.y;
        z = boxCollider.bounds.center.z;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if(null == itemList || itemList.Length <= 0)
        {
            return;
        }
        if(null == playerChar)
        {
            return;
        }
        if(false == canSpawn)
        {
            return;
        }

        for(int i = 0; i < itemList.Length; i++)
        {
            ItemInfomation currentItem = itemList[i];
            if(Time.timeSinceLevelLoad - currentItem.lastSpawnTime >= (currentItem.spawnRate - currentItem.modifiedSpawnRate))
            {
                currentItem.lastSpawnTime = Time.timeSinceLevelLoad;

                float spawnX;
                if(currentItem.playerPosDelta != 0)
                {
                    spawnX = UnityEngine.Random.Range(playerChar.position.x - currentItem.playerPosDelta, playerChar.position.x + currentItem.playerPosDelta);
                    spawnX = Mathf.Clamp(spawnX, minX, maxX);
                }
                else
                {
                    spawnX = UnityEngine.Random.Range(minX, maxX);
                }


                Vector3 spawnPosition = new Vector3(spawnX, y, playerChar.position.z);

                GameObject spawnObj = Instantiate<GameObject>(currentItem.prefab);
                spawnObj.transform.position = spawnPosition;
                spawnObj.GetComponent<DropItem>().Slowness = currentItem.fallingSlowness - currentItem.modifiedFallingSlowness;
            }

            if(currentItem.increaseSpawnRate != 0 && Time.timeSinceLevelLoad - currentItem.lastIncreaseSpawnRate >= currentItem.increaseSpawnRate)
            {
                currentItem.lastIncreaseSpawnRate = Time.timeSinceLevelLoad;

                currentItem.modifiedSpawnRate = Mathf.Clamp(currentItem.modifiedSpawnRate + 0.01f, 0.02f, 100);
                currentItem.modifiedFallingSlowness = Mathf.Clamp(currentItem.modifiedFallingSlowness + 0.01f, -10, 100);
            }
            
        }
	}
    
    

    public void SetSpawnSetting(bool isEnable)
    {
        canSpawn = isEnable;
        if(true == isEnable)
        {
            for(int i =0; i < itemList.Length; i++)
            {
                ItemInfomation currentItem = itemList[i];
                currentItem.modifiedSpawnRate = 0;
                currentItem.lastSpawnTime = currentItem.spawnRate;
                currentItem.lastIncreaseSpawnRate = currentItem.increaseSpawnRate;
                currentItem.modifiedFallingSlowness = 0;
            }
        }
    }
}
