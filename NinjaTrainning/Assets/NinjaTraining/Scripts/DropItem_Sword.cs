﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropItem_Sword : DropItem
{
    protected override void OnHitGround()
    {
        Rigidbody rg = GetComponent<Rigidbody>();
        rg.useGravity = false;
        rg.velocity = Vector3.zero;
        BoxCollider bc = GetComponent<BoxCollider>();
        bc.enabled = false;
        Destroy(gameObject, 3);
        hasTouchGround = true;
        gameObject.GetComponent<ConstantForce>().enabled = false;
    }

    protected override void OnHitPlayer()
    {
        if(true == hasTouchGround)
        {
            return;
        }
        Destroy(gameObject);
        //NJ_GameController.Current.OnGameOver();
        MainCharacterController player = GameObject.FindObjectOfType<MainCharacterController>();
        if (null == player)
        {
            return;
        }
        player.OnGetHit();
    }
}
