﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUD_RotateFlyOut : MonoBehaviour
{
    private RectTransform rectTransform;
    private Image img;
    private bool canFlyOut = false;
    private bool canFlyIn = false;

    public float rotateSpeed = -2f;
    public float moveSpeed = 2f;
    public float timeTillDeath = 2f;
    public float posFlyInStart = 2000.0f;

    public bool IsVisible
    {
        get
        {
            if(null == rectTransform)
            {
                Initialize();
            }

            return rectTransform.anchoredPosition.x == 0 && img.enabled;
        }
    }
    
	// Use this for initialization
	void Start ()
    {
        Initialize();
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (true == canFlyOut)
        {
            if (rectTransform.anchoredPosition.x >= posFlyInStart)
            {
                canFlyOut = false;
                rectTransform.anchoredPosition = new Vector2(posFlyInStart, 0);
            }
            else
            {
                rectTransform.anchoredPosition = new Vector2(rectTransform.anchoredPosition.x + moveSpeed, rectTransform.anchoredPosition.y);
                rectTransform.Rotate(0, 0, rotateSpeed);
            }
        }
        if(true == canFlyIn)
        {
            if(rectTransform.anchoredPosition.x <= 0)
            {
                canFlyIn = false;
                rectTransform.anchoredPosition = new Vector2(0, 0);
            }
            else
            {
                rectTransform.anchoredPosition = new Vector2(rectTransform.anchoredPosition.x + moveSpeed * -1, rectTransform.anchoredPosition.y);
                rectTransform.Rotate(0, 0, rotateSpeed * - 1);
            }
        }
	}

    private void Initialize()
    {
        if (null == rectTransform)
        {
            rectTransform = GetComponent<RectTransform>();
        }
        if(null == img)
        {
            img = GetComponent<Image>();
            img.enabled = false;
        }
    }

    public void PlayFlyOutAnimation()
    {
        //Initialize();
        img.enabled = true;
        canFlyOut = true;
        //Destroy(gameObject, timeTillDeath);
    }

    public void PlayFlyInAnimation()
    {
        //Initialize();
        rectTransform.anchoredPosition = new Vector2(posFlyInStart, rectTransform.anchoredPosition.y);
        img.enabled = true;
        canFlyIn = true;
    }
}
