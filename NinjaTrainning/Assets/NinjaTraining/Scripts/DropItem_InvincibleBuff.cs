﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropItem_InvincibleBuff : DropItem
{
    protected override void OnHitGround()
    {
        Rigidbody rg = GetComponent<Rigidbody>();
        rg.useGravity = false;
        rg.velocity = Vector3.zero;
        hasTouchGround = true;
        gameObject.GetComponent<ConstantForce>().enabled = false;
    }

    protected override void OnHitPlayer()
    {
        //Debug.Log("On hit player");
        MainCharacterController player = GameObject.FindObjectOfType<MainCharacterController>();
        if (null == player)
        {
            return;
        }
        Destroy(gameObject);
        player.OnPickUpInvincibleBuff();
    }
}
