﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Facebook.Unity;
//using Facebook.MiniJSON;

public class FBLogIn : MonoBehaviour
{
	// Use this for initialization
	void Start ()
    {
        if (!FB.IsInitialized)
        {
            FB.Init(InitCallback, OnHideUnity);
        }
        else
        {
            FB.ActivateApp();
        }
    }
    
    private void InitCallback()
    {
        if (FB.IsInitialized)
        {
            FB.ActivateApp();

            if (false == FB.IsLoggedIn)
            {
                List<string> permission = new List<string>() { "public_profile", "email" };
                FB.LogInWithReadPermissions(permission, OnLogInCallback);
            }
            else
            {
                FB.API("me?fields=name", HttpMethod.GET, GetUsernameCallback);
                //NJ_GameController.Current.LoadGameScene();
                SceneManager.LoadScene(1);
                //StartCoroutine(NJ_GameController.Current.LoadGameScene());
            }

        }
        else
        {
            FB.Init(InitCallback, OnHideUnity);
        }
    }

    private void GetUsernameCallback(IGraphResult callbackResult)
    {
        PlayerPrefs.SetString(NJ_Library.fbUserName, callbackResult.ResultDictionary["name"].ToString());
    }


    private void OnHideUnity(bool isGameShow)
    {
        if (false == isGameShow)
        {
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;
        }
    }


    private void OnLogInCallback(ILoginResult result)
    {
        if (FB.IsLoggedIn)
        {
            FB.API("me?fields=name", HttpMethod.GET, GetUsernameCallback);
            //NJ_GameController.Current.LoadGameScene();
            SceneManager.LoadScene(1);
            //StartCoroutine(NJ_GameController.Current.LoadGameScene());
        }
        else
        {
            Debug.Log("Cancelled login");
        }
    }
}
