﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropItem : MonoBehaviour
{

    protected bool hasTouchGround;
    private float slowness;
    public float Slowness
    {
        set
        {
            slowness = value;
            gameObject.GetComponent<ConstantForce>().force = new Vector3(0, slowness, 0);
        }
        get
        {
            return slowness;
        }
    }

    // Use this for initialization
    void Start()
    {
        hasTouchGround = false;

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Terrain")
        {
            OnHitGround();
        }
        if (other.gameObject.tag == "Player")
        {
            OnHitPlayer();
        }
    }
    
    protected virtual void OnHitPlayer()
    {

    }   

    protected virtual void OnHitGround()
    {

    }
}
